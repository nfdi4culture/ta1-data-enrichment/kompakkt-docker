# Main server configuration for HTTP
server {
    listen 80;
    server_name {{ server_name }};
    include mime.types;
    resolver 127.0.0.11 valid=5m;

    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header X-Forwarded-Proto $scheme;

    location / {
        proxy_pass http://repo;
    }

    # Redirect Q Numbers to the wikibase server
    location ~ ^/Q[0-9]+ {
        rewrite ^/(.*)$ http://wb.$host/wiki/Item:$1 redirect;
    }

    location /viewer/ {
        proxy_pass http://viewer;
    }

    location /server/ {
        client_max_body_size 1024m; # Limit set for uploads to server
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $http_host;
        proxy_pass http://server:8080;
    }
}
