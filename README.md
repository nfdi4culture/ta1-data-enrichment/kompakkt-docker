# Semantic Kompakkt

## Roadmap

A high level roadmap can be [viewed here](https://docs.google.com/spreadsheets/d/11nCNq9yXWEzol-lvFzmLdfth57_aNQKCvA01vqZAzFI/edit?usp=sharing).
For more granular development, follow the issues and the milestones in this repository and the TA1 sub-group.

## Description

This repository provides docker configuration to launch complimentary Kompakkt and Wikibase services.

Semantic Kompakkt consists of the following services:

| Service | Description | URL | Build Status |
| --- | --- | --- | --- |
| Server | Handles the API requests and communicates with wikibase | https://gitlab.com/nfdi4culture/ta1-data-enrichment/kompakkt-server | [![pipeline status](https://gitlab.com/nfdi4culture/ta1-data-enrichment/kompakkt-server/badges/develop/pipeline.svg)](https://gitlab.com/nfdi4culture/ta1-data-enrichment/kompakkt-server/commits/develop) |
| Repo | Serves the Main UI | https://gitlab.com/nfdi4culture/ta1-data-enrichment/kompakkt-repo | [![pipeline status](https://gitlab.com/nfdi4culture/ta1-data-enrichment/kompakkt-repo/badges/develop/pipeline.svg)](https://gitlab.com/nfdi4culture/ta1-data-enrichment/kompakkt-repo/commits/develop) |
| Viewer | Serves the 3D Viewer | https://gitlab.com/nfdi4culture/ta1-data-enrichment/kompakkt-viewer | [![pipeline status](https://gitlab.com/nfdi4culture/ta1-data-enrichment/kompakkt-viewer/badges/develop/pipeline.svg)](https://gitlab.com/nfdi4culture/ta1-data-enrichment/kompakkt-viewer/commits/develop) |
| Common | Contains the common Interfaces | https://gitlab.com/nfdi4culture/ta1-data-enrichment/kompakkt-common | Does not have a build status |

## Getting started

### Production Version

To run Kompakkt and Wikibase, navigate to the kompakkt-docker directory and run

```bash
cp sample.env .env   
```

Edit the .env file to your needs. Then run

```bash
docker-compose -f docker-compose.kompakkt.yml -f docker-compose.wikibase.yml up -d
```

The following services will be available

[//]: <> (TODO: Following table should be reworked, since we're using an nginx proxy now)

| Service | Address |
| --- | --- |
| Kompakkt | http://localhost:8080 |
| Wikibase | http://localhost:8900 |
| Query Service | http://localhost:8834 |

Wikibase data can be overwritten with a base snapshot using the following script.

```bash
chmod +x wikibase-write-snapshot.sh
./wikibase-write-snapshot.sh
```


### Development Version

To start up a version where you have the abilty to hot reload the application and edit the code on the fly, run

```bash
git clone --recursive -j8 https://gitlab.com/nfdi4culture/ta1-data-enrichment/semantic-kompakkt.git
cd semantic-kompakkt/
cp sample.env .env
chmod +x scripts/init.sh
./scripts/init.sh
```

Now you have to edit the .env file and set the needed environment variables. For the development version, most of the variables can be left as they are.

```bash
docker-compose -f docker-compose.kompakkt.local-dev.yml -f docker-compose.wikibase.yml up -d
```

Please raise an issue if you have any problems with the development version.

## Patching from GitHub

You can use the `patchfromgh.sh` script to apply patches from the original Kompakkt repositories to the NFDI4Culture repositories.

```bash
chmod +x scripts/patchfromgh.sh
./scripts/patchfromgh.sh -u <PR_URL> [-b <OPT_BRANCH>]
```

Call the script with the `-h` flag to get more information.

### Resolving conflicts

It's pretty certain that there will be conflicts when applying patches. To resolve them, you have to do the following:

There will be .rej files in the repositories. These files contain the conflicting parts of the patch. You have to manually resolve the conflicts and remove the .rej files.

