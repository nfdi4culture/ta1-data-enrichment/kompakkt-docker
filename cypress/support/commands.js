// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import 'cypress-file-upload';

Cypress.Commands.add('login', (username, password) => {
    cy.get('#login').click()
    cy.get('input[name="username"]').type(username)
    cy.get('input[name="password"]').type(password)
    cy.get('button[type="submit"]').click()
})

Cypress.Commands.add('loginRequest', (username, password) => {
    cy.request({
        method: 'POST',
        url: 'https://kompakkt.local/server/user-management/login',
        body: {
            username: username,
            password: password,
        },
        headers: {
            'Content-Type': 'application/json',
        },
    }).then((response) => {
        // Save the session cookie
        const sessionCookie = response.headers['set-cookie'][0].split(';')[0];
        cy.setCookie('session', sessionCookie);

        // Ensure the login was successful
        expect(response.status).to.eq(200);
        // reload the page to ensure the cookie is set
        cy.reload();
    });
});



Cypress.Commands.add('register', (username = null, password = null) => {
    const rand_num = Math.floor(Math.random() * 100000)
    const randomUsername = username ?? 'user' + rand_num;
    const randomPassword = password ?? 'test34567';
    const randomEmail = username ? username + '@kompakkt.local' : 'test' + rand_num + '@kompakkt.local';
    const randomGivenName = 'Test';
    const randomLastName = 'User';

    cy.get('#register').click()
    cy.wait(500)
    cy.get('input[formcontrolname="username"]').type(randomUsername)
    cy.get('input[formcontrolname="password"]').type(randomPassword)
    cy.get('input[formcontrolname="passwordRepeat"]').type(randomPassword)
    cy.get('input[name="mail"]').type(randomEmail)
    cy.get('input[name="prename"]').type(randomGivenName)
    cy.get('input[name="surname"]').type(randomLastName)
    cy.get('button[type="submit"]').click()
    cy.contains('Logged in as ' + randomGivenName + ' ' + randomLastName)
})

Cypress.Commands.add('iframe', { prevSubject: 'element' }, ($iframe) => {
    return new Cypress.Promise(resolve => {
        $iframe.on('load', () => {
            resolve($iframe.contents().find('body'));
        });
    });
});