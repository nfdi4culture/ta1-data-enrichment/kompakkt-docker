// define login variables

import { logins } from "../fixtures/logins"

describe('Kompakkt Login', () => {
  beforeEach(() => {
    cy.visit('https://kompakkt.local/')
  })

  it('should not login', () => {
    cy.login(logins.wrong.username, logins.wrong.password)
    cy.contains('Failed to login.')
  })

  it('register', () => {
    cy.register(logins.correct.username, logins.correct.password)
    cy.contains('Logout')
  })

  it('should login', () => {
    cy.login(logins.correct.username, logins.correct.password)
    cy.contains('Logout')
  })
})