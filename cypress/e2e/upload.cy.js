import { logins } from "../fixtures/logins"

describe('Kompakkt Upload', () => {
    beforeEach(() => {
        cy.visit('https://kompakkt.local/')
    })
    it('should upload', () => {
        const rand_num = Math.floor(Math.random() * 100000)
        // we need to listen to the postMessage event
        // so the viewer can tell the repo when its finished
        cy.window().then((win) => {
            win.top.addEventListener("message", (e) => {
                win.postMessage({ ...e.data }, '*');
            })
        })
        cy.loginRequest(logins.correct.username, logins.correct.password)
        cy.get('#tib-logo').click()
        cy.get('#fileupload').attachFile('Icosidodecahedron.stl')
        cy.get('#begin-upload').should('not.be.disabled')
        cy.get('#begin-upload').click()
        cy.get('#next-upload', { timeout: 10000 }).click()
        // check if iframe is visible
        cy.get('#viewer-frame', { timeout: 10000 })
            .should('be.visible', { timeout: 10000 })
            .then($iframe => {
                cy.wait(3000)
                const $body = $iframe.contents().find('body')
                cy.wrap($body).contains('Next').click()
                cy.wrap($body).contains('Yes').click()
                cy.wrap($body).contains('Next').click()
                cy.wrap($body).contains('Yes').click()
                cy.wrap($body).contains('Save').click()

            })
        // now we fill out the form
        cy.get('input[name="title"]').type('Icosidodecahedron ' + rand_num)
        cy.get('input[name="description"]').type('Icosidodecahedron description ' + rand_num)
        cy.contains('Related Agents').click()
        cy.get('input[name="title"]').type('TIB')
        // sever results with the class rsult-description. pick the first one
        cy.get('.result-description').first().click()
        cy.contains('Rightsowner').click()
        cy.contains('Creator').click()
        cy.get('#addPersonButton').click()
        cy.contains('License').click()
        cy.get('.mat-radio-container').first().click()
        // wait until for next button vibility has changed to visible
        cy.contains('Next', { timeout: 10000, force: true }).click({ force: true })
        // get "Finish" inside class "wizard-step-buttons" and click it
        cy.get('.wizard-step-buttons').contains('Finish').scrollIntoView().should('be.visible').click()
    })

})