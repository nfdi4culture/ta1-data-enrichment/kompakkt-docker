import os
import subprocess
from dotenv import load_dotenv
import click
import shutil

# Load environment variables from .env file
load_dotenv('.env')
MONGO_USERNAME=os.getenv("MONGO_USERNAME")
MONGO_PASSWORD=os.getenv("MONGO_PASSWORD")

SUBPROJECTS = {
    "server": "kompakkt-server",
    "repo": "kompakkt-repo",
    "viewer": "kompakkt-viewer"
}

def remove_node_modules(subproject_dir):
    node_modules_path = os.path.join(subproject_dir, "node_modules")
    if os.path.exists(node_modules_path):
        shutil.rmtree(node_modules_path)

def run_docker_compose(*args):
    docker_compose_file = ["-f", "docker-compose.kompakkt.local-dev.yml", "-f", "docker-compose.wikibase.yml"]
    # check if "docker compose" or "docker-compose" is available
    print("Checking if 'docker compose' is available...")
    try:
        subprocess.run(["docker", "compose", "--help"], check=True)
        print("'docker compose' is available.")
        subprocess.run(["docker", "compose", *docker_compose_file, *args])
    except subprocess.CalledProcessError:
        print("'docker compose' is not available.")
        print("Checking if 'docker-compose' is available...")
        try:
            subprocess.run(["docker-compose", "--help"], check=True)
            print("'docker-compose' is available.")
            subprocess.run(["docker-compose", *docker_compose_file, *args])
        except subprocess.CalledProcessError:
            print("'docker-compose' is not available.")
            print("Please install 'docker-compose' or 'docker compose' to use this script.")
            exit(1)



def run_mongo(*args):
    print("Username: ", MONGO_USERNAME)
    print("Password: ", MONGO_PASSWORD)
    print("this would be the command we run: mongosh -u " + MONGO_USERNAME + " -p " + MONGO_PASSWORD + " " + " ".join(list(args)))
    subprocess.run(["mongosh", "-u", MONGO_USERNAME, "-p", MONGO_PASSWORD, *args])


def run_mongoexport(*args):
    subprocess.run(["mongoexport"] + list(args))


def run_mongoimport(*args):
    subprocess.run(["mongoimport"] + list(args))

def add_standard_user():
    standard_username = "standard_user"
    standard_password = "standard_password"
    standard_role = "uploader"
    run_add_user(standard_username, standard_password, standard_role)

def run_add_user(username, password, role):
    user_exists = run_mongo('--quiet', '--eval', f'db.users.findOne({{username: "{username}"}})', 'accounts')
    user_exists = user_exists.strip()

    if not user_exists:
        run_mongo('--quiet', '--eval', f'db.users.insertOne({{username: "{username}", role: "{role}"}})', 'accounts')
        run_mongo('--quiet', '--eval', f'db.passwords.insertOne({{username: "{username}", password: {{passwordHash: "{password}"}}}})', 'accounts')
        print(f"User '{username}' created.")
    else:
        print(f"User '{username}' already exists.")


def checkout_branch_and_update_submodules(subproject_dir, branch):
    subprocess.run(["git", "checkout", branch], cwd=subproject_dir)
    subprocess.run(["git", "submodule", "update", "--init", "--recursive"], cwd=subproject_dir)

def checkout_branch_for_all_subprojects(branch):
    for subproject_name, subproject_dir in SUBPROJECTS.items():
        print(f"Checking out branch '{branch}' and updating submodules for {subproject_name} ({subproject_dir})...")
        checkout_branch_and_update_submodules(subproject_dir, branch)
    print("All subprojects have been updated to the specified branch.")

def update_subprojects_standalone(clean_install=False):
    for subproject_name, subproject_dir in SUBPROJECTS.items():
        path = subproject_dir
        os.chdir(path)

        if path in ["kompakkt-repo", "kompakkt-viewer"]:
            if not os.path.exists("src/environments/environment.prod.ts"):
                shutil.copy("src/environments/environment.prod.ts.sample", "src/environments/environment.prod.ts")
            if not os.path.exists("src/environments/environment.ts"):
                shutil.copy("src/environments/environment.ts.sample", "src/environments/environment.ts")

        for subproject_name, subproject_dir in SUBPROJECTS.items():
            print(f"Updating {subproject_name} ({subproject_dir})...")
            subprocess.run(["git", "pull"], cwd=subproject_dir)
            if clean_install:
                print(f"Removing 'node_modules' folder for {subproject_name} ({subproject_dir})...")
                remove_node_modules(subproject_dir)
            subprocess.run(["npm", "i"], cwd=subproject_dir)
        print("All subprojects have been updated.")

# Bring kompakkt down with volumes, then bring it up again and run remove.sh in the current directory
def reset_kompakkt():
    run_docker_compose("down", "-v")
    run_docker_compose("up", "-d")
    subprocess.run(["./remove.sh"], cwd=".")


@click.group()
def cli():
    pass


@cli.command()
@click.argument("subcommand", nargs=-1, type=click.STRING)
def default(subcommand):
    """Run a docker-compose command."""
    run_docker_compose(*subcommand)


@cli.command()
@click.argument("output_file", type=click.Path())
def db_backup(output_file):
    """Backup the database to a file."""
    mongo_url = os.getenv("MONGO_URL")
    mongo_database = os.getenv("MONGO_DATABASE")
    run_mongoexport("--uri", mongo_url, "--db", mongo_database, "--collection", "statements", "--out", output_file)


@cli.command()
@click.argument("input_file", type=click.Path(exists=True))
def db_restore(input_file):
    """Restore the database from a file."""
    mongo_url = os.getenv("MONGO_URL")
    mongo_database = os.getenv("MONGO_DATABASE")
    run_mongoimport("--uri", mongo_url, "--db", mongo_database, "--collection", "statements", "--file", input_file)


@cli.command()
@click.argument("services", nargs=-1, type=click.STRING)
def logs(services):
    """Show the logs for server, viewer, and repo."""
    if not services:
        services = list(SUBPROJECTS.values())

    run_docker_compose("logs", "-f", *services)

@cli.command()
@click.option("--save-dev", is_flag=True, help="Run 'npm i --save-dev' after updating.")
@click.option("--clean-install", is_flag=True, help="Remove 'node_modules' folder and perform a clean install.")
def update_subprojects(save_dev, clean_install):
    """Update all subprojects by running 'git pull' and 'npm i'."""
    update_subprojects_standalone(clean_install)
    if save_dev:
        for subproject_name, subproject_dir in SUBPROJECTS.items():
            print(f"Running 'npm i --save-dev' for {subproject_name} ({subproject_dir})...")
            subprocess.run(["npm", "i", "--save-dev"], cwd=subproject_dir)

@cli.command()
@click.argument("branch", type=click.STRING)
@click.option("--clean-install", is_flag=True, help="Remove 'node_modules' folder and perform a clean install.")
def checkout_and_install(branch, clean_install):
    """Checkout a specific branch in all subprojects, update submodules, and run 'npm i --save-dev'."""
    checkout_branch_for_all_subprojects(branch)
    update_subprojects_standalone(clean_install)

@cli.command()
@click.pass_context
def add_standard_user(ctx):
    """Add a standard user to the database."""
    standard_username = "standard_user"
    standard_password = "standard_password"
    standard_role = "uploader"
    run_add_user(standard_username, standard_password, standard_role)

@cli.command()
@click.argument("username", type=click.STRING)
@click.argument("password", type=click.STRING)
@click.argument("role", type=click.STRING)
def add_user(username, password, role):
    """Add a user to the database."""
    run_add_user(username, password, role)

@cli.command()
def reset():
    """Reset the kompakkt environment by removing all containers, volumes, and images."""
    reset_kompakkt()

if __name__ == "__main__":
    cli()
