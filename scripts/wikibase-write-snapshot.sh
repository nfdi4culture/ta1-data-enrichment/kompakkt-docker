# Get the absolute path of the SQL file
abs_path=$(readlink -f $1)

# Copy the SQL file to the Docker container
docker cp $abs_path wikibase_mariadb:/tmp/

# FOr the USER/PASSWORD read in the .env variables in .env MW_ADMIN_USER and MW_ADMIN_PASS
# read in the .env file:

# Now run the MySQL command inside the container to read from the copied SQL file.
# Using /tmp here assuming that's where you've copied the file.
# Extract variables from the container's environment

MYSQL_USER=$(docker exec wikibase_mariadb env | grep MYSQL_USER | cut -d'=' -f2)
MYSQL_PASS=$(docker exec wikibase_mariadb env | grep MYSQL_PASS | cut -d'=' -f2)
MYSQL_DATABASE=$(docker exec wikibase_mariadb env | grep MYSQL_DATABASE | cut -d'=' -f2)

echo $DB_USER
# Continue as before but now using the extracted variables
docker exec -it wikibase_mariadb bash -c "mysql -u $MYSQL_USER -p$MYSQL_PASS $MYSQL_DATABASE < /tmp/$(basename $abs_path)"