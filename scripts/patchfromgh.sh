#!/usr/bin/env bash

# set options
set -o errexit # exit on error
set -o pipefail # exit on pipe error

printError() {
    echo "ERR: $1" >&2
} 

printWarning() {
    echo "WARN: $1"
}

usage() {
    echo "Usage: $0 -u <PR_URL> [-b <OPT_BRANCH>]"
    echo "  -u | --url <PR_URL>         The GitHub Pull Request URL"
    echo "  -b | --branch <OPT_BRANCH>  (Optional) The branch to apply the patch to"
    echo "  -h | --help                 Show this help"
    echo "";
    echo "";
    echo "Description:"
    echo "";
    echo "You can use this script to apply PRs from GitHub to the Kompakkt repositories."
    echo "It will create a new branch from the 'develop' branch inside the folder"
    echo ""
    echo "Example: ./patchfromgh.sh -u https://github.com/Kompakkt/Repo/pull/6" -b "mybranch"
    echo ""
    echo "This will create a new branch called 'mybranch' from the 'develop' branch"
    echo "If the Branch does already exist, it will be checked out."
    echo "The patch will be applied to the new branch."
    echo ""
    echo "If you don't specify a branch name, the branch name will be the PR title."
    echo ""
    echo "Example: ./patchfromgh.sh -u https://github.com/Kompakkt/Repo/pull/6"
    echo ""
    echo "This will create a new branch called '<GitHub PR title>' from the 'develop' branch"
    exit 0
}

# Get the parameters
while [ "$1" != "" ]; do
    case $1 in
        -u | --url )            shift
                                PR_URL=$1
                                ;;
        -b | --branch )         shift
                                OPT_BRANCH=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done


# Check if the URL is set
if [ -z "$PR_URL" ]; then
    echo "You need to specify a GitHub pull request URL."
    echo "Use --help or -h for more information."
    exit 1
fi

# URL can be pr or commit, store it in "pull" or "commit" in new Variable
if [[ $PR_URL == *"pull"* ]]; then
    PR_URL_TYPE="pull"
elif [[ $PR_URL == *"commit"* ]]; then
    PR_URL_TYPE="commit"
else
    echo "The URL is not a valid GitHub pull request URL."
    echo "Use --help or -h for more information."
    exit 1
fi

# Get the PR or Hash number behind https://github.com/Kompakkt/Repo/pull/ or https://github.com/Kompakkt/Repo/commit/.
# we need to use sed remove all the characters before the last slash
PR_NUMBER=$(echo $PR_URL | sed 's/.*\///')

# Get everything after github.com/ in the URL
GH_URL=$(echo $PR_URL | sed 's/.*github.com\///')

# Get the pull request title from the GitHub API
PR_TITLE=$(curl -s $PR_URL | grep -oP '(?<=<title>).*?(?=</title>)' | sed 's/ - GitHub$//')

# Check if patch/ directory exists
if [ ! -d patch ]; then
    mkdir patch
fi

# Get the patch from the GitHub API
if [ "$PR_URL_TYPE" == "pull" ]; then
    curl -s https://patch-diff.githubusercontent.com/raw/$GH_URL.patch > patch/$PR_NUMBER.patch
elif [ "$PR_URL_TYPE" == "commit" ]; then
    curl -s https://github.com/Kompakkt/Viewer/commit/$PR_NUMBER.patch > patch/$PR_NUMBER.patch
fi

# Get the repo folder name from the URL
REPO_FOLDER=$(echo $PR_URL | sed 's/.*\/\(.*\)\/'$PR_URL_TYPE'.*/\L\1/')

# Echo the Details for the User
echo "#############################################"
echo "## Pull Request URL: $PR_URL"
echo "## Pull Request Number: $PR_NUMBER"
echo "## Pull Request Title: $PR_TITLE"
echo "## Repo Folder: $REPO_FOLDER"
echo "#############################################"

# switch to the repo folder
cd kompakkt-$REPO_FOLDER

# check if git status is clean
if [ -z "$(git status --porcelain)" ]; then
    # Working directory clean
    echo "Working directory clean"
else 
    # Uncommitted changes
    printWarning "Uncommitted changes in the working directory. Please commit or stash them before applying the patch."
    cd ..
    exit 1
fi

# check if branch name is set
if [ -z "$OPT_BRANCH" ]; then
    # Branch name is not set, use PR title and put "GH/" in front of it, sluggify it and lowercase it
    BRANCH_NAME="GH/(echo $PR_TITLE | sed 's/[^a-zA-Z0-9]/-/g' | tr '[:upper:]' '[:lower:]')"
    echo "Branch name is not set, using PR title: $BRANCH_NAME"
    echo "Do you want to continue, or do you want to specify a branch name?"
    echo "Press 'y' to continue, or 'n' to specify a branch name."
    read -n 1 -r
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        echo "Please specify a branch name:"
        read BRANCH_NAME
        #slugify the branch name
        BRANCH_NAME=$(echo $BRANCH_NAME | sed 's/[^a-zA-Z0-9]/-/g' | tr '[:upper:]' '[:lower:]')
    fi
else
    # Branch name is set, use it
    BRANCH_NAME=$OPT_BRANCH
    echo "Branch name is set: $BRANCH_NAME"
fi

# check if branch exists
if git rev-parse --verify $BRANCH_NAME > /dev/null 2>&1
then
    # Branch exists, checkout
    echo "Branch $BRANCH_NAME exists, checking out..."
    git checkout $BRANCH_NAME
else
    # Branch does not exist, create it
    echo "Branch $BRANCH_NAME does not exist, creating..."
    git checkout -b $BRANCH_NAME develop
fi

# apply the patch
git apply --reject ../patch/$PR_NUMBER.patch

# echo the new branch name & that the patch was applied 
echo "Patch applied to branch $PR_TITLE"

# remove the patch file
rm ../patch/$PR_NUMBER.patch

# switch back directory
cd ..

# end of script