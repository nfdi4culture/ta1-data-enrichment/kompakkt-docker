#!/bin/sh

clone_repository() {
  cd kompakkt-$1
  cp src/environments/environment.prod.ts.sample src/environments/environment.prod.ts
  cp src/environments/environment.ts.sample src/environments/environment.ts  
  npm install --save-dev
  cd ..
}

clone_repository "viewer"
clone_repository "repo"

cd kompakkt-server
npm install --save-dev
cd ..

# first check if /etc/host has the kompakkt.local entry
grep -q "kompakkt.local" /etc/hosts
if [ $? -eq 0 ]; then
  echo "kompakkt.local already in /etc/hosts"
else
  echo "adding kompakkt.local to /etc/hosts"
  echo "127.0.0.1 query.wb.kompakkt.local reconcile.wb.kompakkt.local wb.kompakkt.local kompakkt.local" | sudo tee -a /etc/hosts
fi

git submodule update --init --recursive
git submodule foreach --recursive git fetch

# docker compose -f compose-kompakkt.kompakkt.local-dev.yml -f compose-kompakkt.wikibase.yml up -d

# docker cp ../configuration/wikibase_2023-04-28.sql wikibase_mariadb:/wikibase_2023-04-28.sql
# docker exec -it wikibase_mariadb bash -c 'mysql -u admin -pnotaverygoodpassword my_wiki < /wikibase_2023-04-28.sql'
# docker exec -it wikibase_wikibase bash -c 'php extensions/CirrusSearch/maintenance/UpdateSearchIndexConfig.php'
# docker exec -it wikibase_wikibase bash -c 'php extensions/CirrusSearch/maintenance/ForceSearchIndex.php --skipLinks --indexOnSkip'
# docker exec -it wikibase_wikibase bash -c 'php extensions/CirrusSearch/maintenance/ForceSearchIndex.php --skipParse'
# docker exec -it wikibase_wikibase bash -c 'php maintenance/runJobs.php'
# docker exec -it wikibase_wikibase bash -c 'php maintenance/update.php'


# docker compose -f docker-compose.kompakkt.local-dev.yml -f wikibase/compose.yml up -d

# docker exec wikibase_wikibase php ./extensions/Wikibase/repo/maintenance/dumpRdf.php --server http://wb.kompakkt.local --output /tmp/rdfOutput
# docker cp wikibase_wikibase:/tmp/rdfOutput ./rdfOutput
# docker cp ./rdfOutput wikibase_wdqs:/tmp/rdfOutput
# docker exec wikibase_wdqs ./munge.sh -f /tmp/rdfOutput -d /tmp/mungeOut -- --conceptUri http://wb.kompakkt.local
# docker exec wikibase_wdqs curl "http://localhost:9999/bigdata/namespace/wdq/sparql"  --data-urlencode "update=DROP ALL;"
# docker exec wikibase_wdqs ./loadData.sh -n wdq -d /tmp/mungeOut

