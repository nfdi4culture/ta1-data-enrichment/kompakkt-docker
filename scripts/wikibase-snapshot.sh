MYSQL_USER=$(docker exec wikibase_mariadb env | grep MYSQL_USER | cut -d'=' -f2)
MYSQL_PASS=$(docker exec wikibase_mariadb env | grep MYSQL_PASS | cut -d'=' -f2)
MYSQL_DATABASE=$(docker exec wikibase_mariadb env | grep MYSQL_DATABASE | cut -d'=' -f2)
docker exec -it wikibase_mariadb bash -c "mysqldump -u $MYSQL_USER -p$MYSQL_PASS $MYSQL_DATABASE > "/tmp/$(date +"%Y-%m-%d").sql"
# now copy the SQL file back to the host
docker cp wikibase_mariadb:/tmp/$(date +"%Y-%m-%d").sql .