'''
Script to import external ontology mappings into Wikibase

Note that you will need to setup a new confidential user-only OAuth 1.0 consumer in your Wikibase instance. 
See the Special:OAuthConsumerRegistration/propose page of the Wikibase instance in question. Ensure that
the consumer has the following rights:
 - High-volume editing
 - Edit existing pages

The mappings CSV file should have one column with the external URI one with the property URI and one with the local Wikibase ID:

external_uri,property,wikibase_property
http://cidoc-crm.org/cidoc-crm/E12_Production,owl:equivalentClass,Q29
http://www.cidoc-crm.org/cidoc-crm/E12_Production,owl:equivalentClass,Q26

The best way to create such files is to use SPARQL over the data model TTL file:

PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT (?o AS ?external_uri) ("owl:equivalentClass" AS ?property) (REPLACE(?note, "Wikibase ID: ", "") as ?wikibase_property) WHERE {
  VALUES ?types {
    owl:Class owl:ObjectProperty owl:DatatypeProperty
  }
  ?s a ?types.
  ?s owl:equivalentClass ?o .
  ?s skos:note ?note .
  FILTER(STRSTARTS(?note, "Wikibase ID: "))
}

Usage:

    python import_external_ontology.py <path-to-mappings.csv> <uri-of-wikibase> <client-key> <client-secret> <resource-owner-key> <resource-owner-secret>

Example:

    python import_external_ontology.py mappings.csv https://data.fomu.be CLIENT_KEY CLIENT_SECRET RESOURCE_OWNER_KEY RESOURCE_OWNER_SECRET

Dependencies:
    pip install requests_oauthlib
'''
import csv
import sys

from requests_oauthlib import OAuth1Session

csv_path = sys.argv[1]
wikibase_uri = sys.argv[2]
consumer_key = sys.argv[3]
consumer_secret = sys.argv[4]
access_token = sys.argv[5]
access_secret = sys.argv[6]

csv_data = csv.DictReader(open(csv_path, encoding='utf-8'))
mediawiki = OAuth1Session(consumer_key, client_secret=consumer_secret, resource_owner_key=access_token, resource_owner_secret=access_secret)

endpoint = wikibase_uri + '/w/rest.php/wikibase-rdf/v0/mappings/'
headers = {'Content-Type': 'application/json'}
for row in csv_data:
    url = endpoint + row['wikibase_property']
    data = [
        {
            'predicate': row['property'],
            'object': row['external_uri']
        }
    ]

    r = mediawiki.post(url, json=data, headers=headers)
    print('Imported mapping for ' + row['wikibase_property'])
