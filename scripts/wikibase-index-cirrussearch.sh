docker exec -it wikibase_elasticsearch curl -X PUT -H "Content-Type: application/json" 127.0.0.1:9200/_all/_settings -d '{"index.blocks.read_only_allow_delete": null}'
docker exec -it wikibase_wikibase bash -c 'php extensions/CirrusSearch/maintenance/UpdateSearchIndexConfig.php'
docker exec -it wikibase_wikibase bash -c 'php extensions/CirrusSearch/maintenance/ForceSearchIndex.php --skipLinks --indexOnSkip'
docker exec -it wikibase_wikibase bash -c 'php extensions/CirrusSearch/maintenance/ForceSearchIndex.php --skipParse'
docker exec -it wikibase_wikibase bash -c 'php maintenance/runJobs.php'
