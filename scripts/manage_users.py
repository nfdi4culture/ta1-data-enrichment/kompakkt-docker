import click
import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# Default values
default_values = {
    'server': 'https://kompakkt.local/server',
    'register_endpoint': '/user-management/register',
    'toggle_workshop_mode_endpoint': '/admin/toggleworkshopmode',
    'fullname': 'Test User',
    'password': 'test34567',
    'mail': 'admin@kompakkt.local',
    'prename': 'Test',
    'surname': 'User',
    'username': 'admin'
}

@click.group()
def cli():
    pass

# Function to create a user
def create_user_callback(server, register_endpoint, fullname, password, mail, prename, surname, username):
    url = f"{server}{register_endpoint}"
    json_data = {
        'fullname': fullname,
        'password': password,
        'mail': mail,
        'prename': prename,
        'surname': surname,
        'username': username
    }
    response = requests.post(url, json=json_data, verify=False)
    if response.status_code == 200:
        click.echo("User created successfully.")
    else:
        click.echo(f"Error creating user. Response: {response.text}")


# Function to toggle workshop mode
def toggle_workshop_mode_callback(server, toggle_workshop_mode_endpoint, admin_username, admin_password):
    url = f"{server}{toggle_workshop_mode_endpoint}"
    json_data = {
        'username': admin_username,
        'password': admin_password
    }
    response = requests.post(url, json=json_data, verify=False)
    if response.status_code == 200:
        click.echo("Workshop mode toggled successfully.")
    else:
        click.echo(f"Error toggling workshop mode. Response: {response.text}")

# Click commands
@click.command()
@click.option('--server', default=default_values['server'], help='Define the server.')
@click.option('--register-endpoint', default=default_values['register_endpoint'], help='Define the register endpoint.')
@click.option('--fullname', default=default_values['fullname'], help='User full name.')
@click.option('--password', default=default_values['password'], help='User password.')
@click.option('--mail', default=default_values['mail'], help='User email.')
@click.option('--prename', default=default_values['prename'], help='User first name.')
@click.option('--surname', default=default_values['surname'], help='User last name.')
@click.option('--username', default=default_values['username'], help='User username.')
def create_user(server, register_endpoint, fullname, password, mail, prename, surname, username):
    create_user_callback(server, register_endpoint, fullname, password, mail, prename, surname, username)

@click.command()
@click.option('--server', default=default_values['server'], help='Define the server.')
@click.option('--toggle-workshop-mode-endpoint', default=default_values['toggle_workshop_mode_endpoint'], help='Define the toggle workshop mode endpoint.')
@click.option('--admin-username', default=default_values['username'], help='Admin username.')
@click.option('--admin-password', default=default_values['password'], help='Admin password.')
def toggle_workshop_mode(server, toggle_workshop_mode_endpoint, admin_username, admin_password):
    toggle_workshop_mode_callback(server, toggle_workshop_mode_endpoint, admin_username, admin_password)

@click.command()
@click.option('--server', default=default_values['server'], help='Define the server.')
@click.option('--register-endpoint', default=default_values['register_endpoint'], help='Define the register endpoint.')
@click.option('--toggle-workshop-mode-endpoint', default=default_values['toggle_workshop_mode_endpoint'], help='Define the toggle workshop mode endpoint.')
def create_multiple_users(server, register_endpoint, toggle_workshop_mode_endpoint):
    user_arr = ["lukas", "lozana"]

    # Define the server
    server_url = server

    # Define the register endpoint
    register_endpoint_url = register_endpoint

    # Define the toggle workshop mode endpoint
    toggle_workshop_mode_endpoint_url = toggle_workshop_mode_endpoint

    # Create a standard user with default values
    create_user_callback(server_url, register_endpoint_url, default_values['fullname'], default_values['password'], default_values['mail'], default_values['prename'], default_values['surname'], default_values['username'])

    # Toggle workshop mode
    toggle_workshop_mode_callback(server_url, toggle_workshop_mode_endpoint_url, default_values['username'], default_values['password'])

    # Create users from the array
    for username in user_arr:
        create_user_callback(server_url, register_endpoint_url, f"{username} Fullname", default_values['password'], f"{username}@kompakkt.local", username, "User", username)

cli.add_command(create_user)
cli.add_command(create_multiple_users)
cli.add_command(toggle_workshop_mode)

if __name__ == '__main__':
    cli()