docker exec wikibase_wikibase sh -c 'php ./extensions/Wikibase/repo/maintenance/dumpRdf.php --server ${WIKIBASE_SERVER} --output /tmp/rdfOutput'
docker cp wikibase_wikibase:/tmp/rdfOutput ./rdfOutput
docker cp ./rdfOutput wikibase_wdqs:/tmp/rdfOutput
docker exec wikibase_wdqs sh -c './munge.sh -f /tmp/rdfOutput -d /tmp/mungeOut -- --conceptUri ${WIKIBASE_SERVER} -vvv'
docker exec wikibase_wdqs curl "http://localhost:9999/bigdata/namespace/wdq/sparql"  --data-urlencode "update=DROP ALL;"
docker exec wikibase_wdqs ./loadData.sh -n wdq -d /tmp/mungeOut
