<?php

# custom sitename
$wgSitename = getenv('WIKIBASE_NAME');

# It will complain $wgServer is not set, but it's fine
# because we are proxying the request to the wiki
# if we would use $wgServer, wikibase would throw a 404


# custom logo
$wgLogo = "$wgResourceBasePath/images/logo.png";

# custom icon
$wgFavicon = "$wgResourceBasePath/images/favicon.ico";

# disable captchas on account creation
$wgCaptchaTriggers['createaccount'] = false;

# allow file upload
$wgEnableUploads = true;

# load LocalMedia extension
wfLoadExtension('WikibaseLocalMedia');
wfLoadExtension('KompakktCacheCleaner');

$wgKompakktSecret = getenv('KOMPAKKT_CACHE_SECRET');
$wgKompakktPath = "http://server:8080";

# Wikibase will cause pain if the values of WIKIBASE_FORMATTER_URL_PROPERTY or WIKIBASE_CANONICAL_URI_PROPERTY points to a non-existing property
# but because we set up the properties post-initilization we can't have them defined here on the first run
# and because it quickly gets complicated in Ansible/Docker to have potential unset variables we assume they are set to "undefined" by default until their properties exist
/* if (isset($_ENV['WIKIBASE_FORMATTER_URL_PROPERTY']) && $_ENV['WIKIBASE_FORMATTER_URL_PROPERTY'] !== "empty") {
    $wgWBRepoSettings['formatterUrlProperty'] = $_ENV['WIKIBASE_FORMATTER_URL_PROPERTY'];
}

if (isset($_ENV['WIKIBASE_CANONICAL_URI_PROPERTY']) && $_ENV['WIKIBASE_CANONICAL_URI_PROPERTY'] !== "empty") {
    $wgWBRepoSettings['canonicalUriProperty'] = $_ENV['WIKIBASE_CANONICAL_URI_PROPERTY'];
} */


# confirm account for account creation request
$wgConfirmAccountRequestFormItems = array(
    'UserName'        => array('enabled' => true),
    'RealName'        => array('enabled' => false),
    'Biography'       => array('enabled' => false, 'minWords' => 5),
    'AreasOfInterest' => array('enabled' => false),
    'CV'              => array('enabled' => false),
    'Notes'           => array('enabled' => false),
    'Links'           => array('enabled' => false),
    'TermsOfService'  => array('enabled' => false),
);

wfLoadExtension('ConfirmAccount');

$wgWikibaseRdfPredicates = [
    'owl:sameAs',
    'owl:equivalentClass',
    'owl:equivalentProperty',
];

wfLoadExtension('WikibaseRDF');

$wgGroupPermissions['bureaucrat']['confirmaccount-notify'] = true;
$wgGroupPermissions['*']['createaccount'] = false;
$wgGroupPermissions['*']['edit'] = false;

# permissive password generation

#$wgPasswordPolicy = ['policies' => ['default' => []], 'checks' => []];

if (getenv('MAIL_HOST')) {
    # set smtp
    $wgSMTP = [
        'host' => getenv('MAIL_HOST'),
        'port' => getenv('MAIL_PORT'),
    ];
}
